/*
 * Copyright © 2013 Thibaut Paumard
 * A non exclusive license to use, modify and distribute this trivial file
 * without restrictions is hereby granted.
 */
if (!yeti_gsl_included) {
  yeti_gsl_included=1;
  write, format="%s\n", "WARNING: yeti_gsl.i has been been deprecated by gsl.i, please update your code.";
 }
#include "gsl.i"
